// @flow

declare var process: {
  ...Process,
  env: { [key: string]: string | void },
};
