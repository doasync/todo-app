# Todo App

Why Effector?
-------------

Effector is reactive state manager.
It is decentralized, declarative and efficienct.

Effector was created to manage data in complex applications
without danger of inflating a monolithic central store.
It provides events/effects and reactive storages.
It has explicit control flow, Flow and TypeScript
type definitions and sufficient API

###Here are some principles:

- application store should be as light as possible - you should not be frightened by the idea that you need to add another store for specific needs

- stores should be freely combined - the idea is that the data that the application will need can be distributed statically by showing in advance how the data will be transformed during the operation of the application

- it should by design eliminate the need for "reselect", notifying about changes only those who need them. This allows you to not worry about the fact that you will have the entire application triggered if you want to extract the state of modal window from React, for example. Concurrently, this means that applications are spared from the problems with performance such as "react-redux" encountered when switching to context

- give opportunity, place, and way to remove any desired business logic from the view, making the components as simple as possible

- independence from controversial concepts - no decorators, no dependencies from reactr/ rxjs, the need to use classes or proxies - none of this is required to manage the state of the application and therefore the api library uses only functions and plain js objects

- predictabile API: a small number of basic principles are reused in various cases, reducing the burden on the user and increasing awareness. Knowing how .watch works in events, you can guess what the .watch function does in front of it.

- the application is easily built from a combination of basic elements and the ability to build new ones

- there is no point in trying to pass everything off for streaming, for reducer or observable. Effector offers a solution to manage the data structure rather than hide it

- the most efficient way to manage state reliably is to describe the interaction of the data declaratively and provide all the dirty work to the automation

Code Analysis Tools
-------------------

We use some open source tools to perform static code analysis:

- ESLint
- Stylelint
- Flow

These tools are automated and doesn't require manual work once they've been set up.
They are also integrated in our continuous integration process.

### ESLint

Linting is a process of checking the source code for stylistic as well as
programmatic errors. ESLint helps to identify some mistakes that are made during coding.
It is also used for adhering best practices and improving code quality and readability.

Our JavaScript code is written in compliance with Airbnb style guide. It is a set of standards that
outline how code should be written and organized and it covers nearly every aspect of JavaScript.
We use ESLint with an Airbnb config and some popular plugins.

You can run ESLint manually with the following command: `npm run lint:js`.

Setup your IDE/Editor to show you ESLint errors and warnings:

###### WebStorm

1. Go to File ➤ Settings / Default Settings ➤ Languages and Frameworks ➤ JavaScript ➤ Code Quality Tools ➤ ESLint
2. Click "Enable" checkbox
3. Select Node interpreter
4. Specify path to eslint package which is inside the project's `node_modules` folder.

###### VSCode

1. Go to: View -> Extension or press CTRL + SHIFT + X
2. Search for "ESLint" extension, click it and press "Install" button
3. Press "Reload" button or reopen the editor

### Stylelint

Stylelint is a linter that helps to avoid errors and enforce conventions in styles.
It understands the latest CSS syntax, parses SCSS, extracts embedded styles from HTML,
markdown and CSS-in-JS template literals.

It is used with "stylelint-config-standard" which extends recommended Stylelint config and
turns on additional rules to enforce the common stylistic conventions found within a handful
of CSS styleguides, including: The Idiomatic CSS Principles, Google's CSS Style Guide and
Airbnb's Styleguide.

You can run Stylelint manually with the following command: `npm run lint:css`.

The setup process for your IDE is similar to ESLint. I'm sure you'll figure it out.

### Flow

Flow is static type checker for JavaScript. It is often compared to TypeScript,
but Flow is not a programming language in itself, but rather a tool. It integrates
better with projects that are using Babel as a transpiler. With Flow
you’ll have much higher type coverage with less type annotations faster
than with TypeScript (on Linux).

Flow helps us to find potential problems with our code earlier, enhances error
detection and improves DX. Flow and React are both Facebook products and they are
used a lot together.

You should configure your IDE to show you Flow errors and warnings,
but you can also run a command line utility: `npm run flow`

Flow docs: https://flow.org/en/docs/

#### Dependencies
```
docker > 18
docker-compose > 1.22
```

#### Dev dependencies
```
node: >= 10.10.0,
npm: >= 6.4.1
```

Usage
------
Use Makefile commands to simplify usage, where possible

To show list of commands from Makefile with their description: 

```bash
make usage
```

To run specific command from Makefile:

```bash
make {COMMAND_NAME}
```

You can find a list of scripts, that can be executed by `make run` command, in the *"scripts"* section of the `package.json` file.
